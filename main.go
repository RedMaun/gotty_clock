package main

import (
	"fmt"
	"os"
	"os/exec"
	"os/signal"
	"syscall"
	"strings"
	"time"
	terminal "github.com/wayneashleyberry/terminal-dimensions"
)


var one = `
   #    
  ##    
 # #    
   #    
   #    
   #    
####### `
var two = ` 
 #####  
#     # 
      # 
 #####  
#       
#       
####### `
var three = ` 
 #####  
#     # 
      # 
 #####  
      # 
#     # 
 #####  `
var four = `
#       
#    #  
#    #  
#    #  
####### 
     #  
     #  `
var five = `
####### 
#       
#       
######  
      # 
#     # 
 #####  `
var six = ` 
 #####  
#     # 
#       
######  
#     # 
#     # 
 #####  `
var seven = `
####### 
#    #  
    #   
   #    
  #     
  #     
  #     `
var eight = ` 
 #####  
#     # 
#     # 
 #####  
#     # 
#     # 
 #####  `
var nine = `
 #####  
#     # 
#     # 
 ###### 
      # 
#     # 
 #####  `
var zero = ` 
  ###   
 #   #  
#     # 
#     # 
#     # 
 #   #  
  ###   `
var split = `
 #  
### 
 #  
    
 #  
### 
 #  `
var space = `
 
 
 
 
 
 
 `
var list = [10]string{zero, one, two, three, four, five, six, seven, eight, nine}
var on, tw, th, fo string

func merge(a, b string, margin int) string {
	af := strings.Split(a, "\n")
	bf := strings.Split(b, "\n")

	an := len(af)
	bn := len(bf)

	ai := 0
	bi := 0
	lf := ""
	for ai < an && bi < bn {
		if ai <= an {
			lf += af[ai]
			ai++
		}
		fmt.Print(strings.Repeat(" ", margin))
		if bi <= bn {
			lf += bf[bi]
			bi++
		}
		lf += "\n"
	}
	return lf
}

var oldtime = ""
var oldx uint = 0
var oldy uint = 0
func main() {
	fmt.Print("\033[?1049h\033[H")
	fmt.Print("\033[?25l")
	SetupCloseHandler()
	for true {
		t := time.Now()
		hour := t.Format("15")
		min := t.Format("04")
		x, _ := terminal.Width()
		y, _ := terminal.Height()
		time.Sleep(500 * time.Microsecond)
		if min != oldtime || x != oldx || y != oldy {
			oldtime = t.Format("04")
			oldx, _ = terminal.Width()
			oldy, _ = terminal.Height()
			time.Sleep(500 * time.Microsecond)
			CallClear()
			on = list[hour[0]-48]
			tw = list[hour[1]-48]
			th = list[min[0]-48]
			fo = list[min[1]-48]
			hours := merge(on, tw, 2)
			gaps := merge(hours, space, 2)
			splitter := merge(gaps, split, 2)
			gaps = merge(splitter, space, 2)
			minutes := merge(th, fo, 2)
			result := merge(gaps, minutes, 2)
			for i := 0; i <= int(oldx)/2-19; i++ {
				result = merge(space, result, 0)
			}
			for i := 0; i <= int(oldy)/2-7; i++ {
				fmt.Print("\n")
			}
			fmt.Print(result)
		}
	}
}

func CallClear() {
		cmd := exec.Command("clear")
		cmd.Stdout = os.Stdout
		cmd.Run()
}


func SetupCloseHandler() {
	c := make(chan os.Signal)
	signal.Notify(c, os.Interrupt, syscall.SIGTERM)
	go func() {
		<-c
		fmt.Print("\033[?25h")
		fmt.Print("\033[?1049l")
		os.Exit(0)
	}()
}
